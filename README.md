# RabbitMQ Sandbox

## Requirements

- Node
- Docker

## Installation

```bash
yarn install
```

## Usage

All the following commands will require a different shell.

### Start RabbitMQ using Docker

```bash
sh rabbitmq.sh
```

### Send task messages to the `tasks` queue

```bash
node src/new-task.js
```

This will send a stingified JSON

```json
{
    "msg": "Task...",
    "id": 1
}
```

The number of dots in the message will simulate the number of seconds to process the task.

### Consume task messages from the `tasks` queue

```bash
node src/worker.js
```

You can spawn as many workers as you want, the messages will be dispatched evenly.

If a worker stops, all non-acknowledged messages will be redispatched to the remaining workers.
