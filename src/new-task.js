const amqp = require('amqplib');

const QUEUE = 'tasks';

(async function() {
    const connection = await amqp.connect(`amqp://localhost`);
    const channel = await connection.createChannel();

    await channel.assertQueue(QUEUE, {
        durable: true,
    });

    let id = 1;

    const interval = setInterval(() => {
        const duration = parseInt(Math.random() * 5 + 5, 10);

        channel.sendToQueue(
            QUEUE,
            Buffer.from(JSON.stringify({ duration, id })),
            {
                persistent: true,
            },
        );
        console.log(` [x] Sent message ${id} | duration = ${duration}`);
        id++;
    }, 0.1 * 1000);

    setTimeout(() => {
        clearInterval(interval);
        connection.close();
    }, 60 * 1000);
})();
