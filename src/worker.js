const amqp = require('amqplib');

const QUEUE = 'tasks';

(async function() {
    const connection = await amqp.connect(`amqp://localhost`);
    const channel = await connection.createChannel();

    await channel.assertQueue(QUEUE, {
        durable: true,
    });

    channel.consume(
        QUEUE,
        function(msg) {
            const { duration, id } = JSON.parse(msg.content.toString());

            console.log(
                ` [x] Received message ${id} | duration = ${duration} second(s)`,
            );

            setTimeout(() => {
                console.log(' [x] Done %s !\n', id);
                channel.ack(msg);
            }, duration * 1000);
        },
        { noAck: false },
    );
})();
